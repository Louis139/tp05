﻿using System;
using Mesozoic;

namespace mesozoicconsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello from Mesozoic!");
            Dinosaur louis, nessie, groschauve, thekairi;

            louis = new Dinosaur("Louis", "Stegausaurus", 12);
            nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            groschauve = new Dinosaur("GrosChauve", "Chauvausaurus", 34);
            thekairi = new Dinosaur("TheKairi78", "Kebabausaure", 22);
            kenny = new Dinosaur("Kenny", "Ramadosaure", 20);


            Console.WriteLine("Présentation de nos dinosaures:");

            Console.WriteLine(thekairi.SayHello());
            Console.WriteLine(thekairi.Roar());
            

            Console.WriteLine(louis.Hug(nessie));
            Console.WriteLine(nessie.SayHello());

            Console.WriteLine(louis.Hug(louis));
            Console.WriteLine(nessie.Hug(louis));

            Console.ReadKey();
        }
    }
}
